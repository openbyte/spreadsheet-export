<?php

namespace OpenbyteSpreadsheetExportTests;

use OpenbyteSpreadsheetExport\ColumnIndex;
use OpenbyteSpreadsheetExport\ColumnLetter;
use PHPUnit\Framework\TestCase;

class ColumnIndexTest extends TestCase
{
    public function testFirstColumnLetter(): void
    {
        $columnLetter = null;
        foreach (ColumnLetter::ALPHABET as $letter) {
            if($columnLetter === null){
                $columnLetter = new ColumnIndex();
            } else {
                $columnLetter->incrementColumnLetter();
            }
            $this->assertSame($letter, (string)$columnLetter);
        }
    }

    private function incrementUntilLastChar(ColumnIndex $columnIndex): ColumnIndex
    {
        for($i = 0; $i < count(ColumnLetter::ALPHABET) - 1; $i++) {
            $columnIndex->incrementColumnLetter();
        }
        return $columnIndex;
    }

    public function testTwoColumnLetter(): void
    {
        $columnLetter = new ColumnIndex();
        $columnLetter = $this->incrementUntilLastChar($columnLetter);

        $this->assertSame("Z", (string)$columnLetter);
        $columnLetter->incrementColumnLetter();
        $this->assertSame("AA", (string)$columnLetter);

        $columnLetter = $this->incrementUntilLastChar($columnLetter);
        $this->assertSame("AZ", (string)$columnLetter);
        $columnLetter->incrementColumnLetter();
        $this->assertSame("BA", (string)$columnLetter);
    }

    public function testThreeColumnLetter(): void
    {
        $columnLetter = new ColumnIndex();
        $columnLetter = $this->incrementUntilLastChar($columnLetter);

        $this->assertSame("Z", (string)$columnLetter);
        $columnLetter->incrementColumnLetter();
        $this->assertSame("AA", (string)$columnLetter);

        for($i = 0; $i < count(ColumnLetter::ALPHABET); $i++) {
            $columnLetter = $this->incrementUntilLastChar($columnLetter);
            $this->assertSame(sprintf("%sZ", ColumnLetter::ALPHABET[$i]), (string)$columnLetter);
            $columnLetter->incrementColumnLetter();
        }

        $this->assertSame("AAA", (string)$columnLetter);
    }
}
