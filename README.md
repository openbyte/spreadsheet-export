# Spreadsheet Export #

This is an easy export library for xlsx, csv and odt formats using Symfony Serializer. Properties to export can be marked using the `\Symfony\Component\Serializer\Annotation\Groups` attribute. 

```
use \Symfony\Component\Serializer\Annotation\Groups;

class User
{
    #[Groups("test")]
    private string $familyName;

    #[Groups("test")]
    private string $firstname;
    
    public function getFamilyName(): string 
    { 
        return $this->familyName; 
    }

    public function getFirstname(): string 
    { 
        return $this->firstname; 
    }
}
```

The export is configured by extending the `AbstractSpreadsheetExport`. The `SpreadsheetBuilder::add` method should be used to add columns of the spreadsheet providing in the first argument the property name and in the second the column label.


```
use OpenbyteSpreadsheetExport\AbstractSpreadsheetExport;

class Export extends AbstractSpreadsheetExport
{
    protected function buildSpreadsheet(SpreadsheetBuilder $builder, array $options): SpreadsheetBuilder
    {
        return $builder
            ->add('familyName', 'Family Name')
            ->add('firstname', 'Firstname')
            ;
    }

    protected function getGroupNames(): array
    {
        return ["test"];
    }
}
```

Using the `ExportFormService::saveSpreadsheetToFile` or `ExportFormService::getSpreadsheetAsResponse` can be used to create the export. The following example will save a csv file to `/tmp/my-csv-export.csv`:

```
use \OpenbyteSpreadsheetExport\ExportFormService;

$listOfUsers = [];

$user = new User();
$user->setFamilyName("John");
$user->setFirstname("Doe");
$listOfUsers[] = $user;

$user = new User();
$user->setFamilyName("Alice");
$user->setFirstname("Doe");
$listOfUsers[] = $user;

$absoluteOsFilePathFolder = '/tmp'
$filenameWithoutExtension = "my-csv-export"

$export = new Export();
$export->setListEntitiesToWrite($listOfUsers);

(new ExportFormService())->saveSpreadsheetToFile(
    SpreadsheetExportFormat::CsvFormat,
    $export,
    sprintf("%s/%s", getcwd(), "test-output"),
    "testfile"
);

```