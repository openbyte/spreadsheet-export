<?php

namespace OpenbyteSpreadsheetExport;

use Closure;

class ColumnConfiguration
{
    public function __construct(
        private ?string $columnTitleLabel = null,
        private ?Closure $formatFunctionOnProperty = null,
        private ?string $phpOfficeFormatCode = null,
        private ?string $unmappedStaticValue = null,
        private ?Closure $unmappedFunctionOnObject = null
    ){
    }

    /**
     * @return string|null
     */
    public function getColumnTitleLabel(): ?string
    {
        return $this->columnTitleLabel;
    }

    /**
     * @return Closure|null
     */
    public function getFormatFunctionOnProperty(): ?Closure
    {
        return $this->formatFunctionOnProperty;
    }

    /**
     * @return string|null
     */
    public function getPhpOfficeFormatCode(): ?string
    {
        return $this->phpOfficeFormatCode;
    }

    /**
     * @return string|null
     */
    public function getUnmappedStaticValue(): ?string
    {
        return $this->unmappedStaticValue;
    }

    /**
     * @return Closure|null
     */
    public function getUnmappedFunctionOnObject(): ?Closure
    {
        return $this->unmappedFunctionOnObject;
    }
}