<?php

namespace OpenbyteSpreadsheetExport;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ReportMultipleSheetService implements ReportInterface
{
    /**
     * @param array<string, AbstractReportSingleSheet> $listOfExports
     */
    public function __construct(private array $listOfExports)
    {
    }

    /**
     * @param array<string, mixed> $options
     * @return Spreadsheet
     */
    public function getSpreadSheet(array $options): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $index = 0;
        foreach ($this->listOfExports as $caption => $export) {
            if ($index === 0) {
                $sheet = $spreadsheet->getActiveSheet();
            } else {
                $sheet = $spreadsheet->createSheet($index);
            }
            $sheet->setTitle(substr($caption, 0, 31));
            $export->writeEntriesToSheet($sheet, $export->getRowsToWrite(), $options);
            $index++;
        }
        return $spreadsheet;
    }

    public function getRowsToWrite(): array
    {
        throw new \Exception('getRowsToWrite should not be called in "ReportMultipleSheetService"');
    }
}
