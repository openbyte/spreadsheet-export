<?php

namespace OpenbyteSpreadsheetExport;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Ods;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ExportOutputService
{
    /**
     * @param SpreadsheetExportFormat $format
     * @param ReportInterface $exportService
     * @param string $absoluteFilePathToSaveFile
     * @param string $filenameWithoutExtension
     * @param array<string, mixed> $spreadsheetOptions
     * @return void
     * @throws \Exception
     */
    public function saveReportToFile(
        SpreadsheetExportFormat $format,
        ReportInterface $exportService,
        string $absoluteFilePathToSaveFile,
        string $filenameWithoutExtension,
        array $spreadsheetOptions = []
    ): void {
        $spreadsheet = $exportService->getSpreadSheet($spreadsheetOptions);
        if($spreadsheet instanceof Spreadsheet) {
            $this->saveSpreadsheetToFile($format, $spreadsheet, $absoluteFilePathToSaveFile, $filenameWithoutExtension);
        } else {
            $index = 0;
            foreach ($spreadsheet as $sp){
                $this->saveSpreadsheetToFile(
                    $format,
                    $sp,
                    $absoluteFilePathToSaveFile,
                    sprintf("%s-%d", $filenameWithoutExtension, $index)
                );
                $index++;
            }
        }
    }

    private function saveSpreadsheetToFile(
        SpreadsheetExportFormat $format,
        Spreadsheet $spreadsheet,
        string $absoluteFilePathToSaveFile,
        string $filenameWithoutExtension,
    ): void {
        $writer = $this->getSpreadsheetWriter($format, $spreadsheet);
        $filename = $this->getFilenameWithExtension($filenameWithoutExtension, $format);
        $filePathToSave = sprintf("%s/%s",
            rtrim($absoluteFilePathToSaveFile, " \t\n\r\0\x0B/"),
            ltrim($filename, " \t\n\r\0\x0B/")
        );
        $writer->save($filePathToSave);
    }

    /**
     * @param SpreadsheetExportFormat $format
     * @param ReportInterface $exportService
     * @param string $filenameWithoutExtension
     * @param array<string, mixed> $spreadsheetOptions
     * @return void
     * @throws \Exception
     */
    public function outputReportRaw(
        SpreadsheetExportFormat $format,
        ReportInterface $exportService,
        string $filenameWithoutExtension,
        array $spreadsheetOptions = []
    ): void {
        $spreadsheet = $exportService->getSpreadSheet($spreadsheetOptions);
        $contentType = $format->getContentType();
        if(!$spreadsheet instanceof Spreadsheet){
            throw new \Exception("multiple file download is not supported yet");
        }
        $writer = $this->getSpreadsheetWriter($format, $spreadsheet);
        $filename = $this->getFilenameWithExtension($filenameWithoutExtension, $format);
        header(sprintf('Content-Type: %s', $contentType));
        header(sprintf('Content-Disposition: attachment;filename="%s"', urlencode($filename)));
        $writer->save('php://output');
        exit();
    }


    /**
     * @param SpreadsheetExportFormat $format
     * @param ReportInterface $exportService
     * @param string $filenameWithoutExtension
     * @param array<string, mixed> $spreadsheetOptions
     * @return Response
     * @throws \Exception
     */
    public function getSpreadsheetAsResponse(
        SpreadsheetExportFormat $format,
        ReportInterface $exportService,
        string $filenameWithoutExtension,
        array $spreadsheetOptions = []
    ): Response {
        $spreadsheet = $exportService->getSpreadSheet($spreadsheetOptions);
        $contentType = $format->getContentType();
        if(!$spreadsheet instanceof Spreadsheet){
            throw new \Exception("multiple file download is not supported yet");
        }
        $writer = $this->getSpreadsheetWriter($format, $spreadsheet);
        $filename = $this->getFilenameWithExtension($filenameWithoutExtension, $format);
        $response = new StreamedResponse();
        $response->headers->set('Content-Type', $contentType);
        $response->headers->set('Content-Disposition', sprintf('attachment;filename="%s"', $filename));
        $response->setPrivate();
        $response->headers->addCacheControlDirective('no-cache', true);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->setCallback(function () use ($writer): void {
            $writer->save('php://output');
        });
        return $response;
    }

    private function getSpreadsheetWriter(
        SpreadsheetExportFormat $format,
        Spreadsheet $spreadsheet
    ): BaseWriter {
        return match ($format) {
            SpreadsheetExportFormat::OpenDocsFormat => new Ods($spreadsheet),
            SpreadsheetExportFormat::ExcelFormat => new Xlsx($spreadsheet),
            default => new Csv($spreadsheet),
        };
    }

    private function getFilenameWithExtension(
        string $filenameWithoutExtension,
        SpreadsheetExportFormat $format
    ): string {
        return sprintf("%s.%s", $filenameWithoutExtension, $format->value);
    }
}