<?php

namespace OpenbyteSpreadsheetExport;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Ods;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\StreamedResponse;

interface ReportInterface
{
    /**
     * @param array<string, mixed> $options
     * @return Spreadsheet|Spreadsheet[]
     * @throws \Exception
     */
    public function getSpreadSheet(array $options): Spreadsheet|array;

    /**
     * @return array<string, mixed>
     */
    public function getRowsToWrite(): array;
}
