<?php

namespace OpenbyteSpreadsheetExport;
use Closure;

class SpreadsheetBuilder
{
    /** @var SpreadsheetColumn[] */
    private array $spreadsheetColumns = [];

    public function add(
        string $propertyName,
        string $columnTitleLabel,
        ?Callable $formatFunctionOnProperty = null,
        ?string $phpOfficeFormatCode = null,
        ?string $unmappedStaticValue = null,
        ?Callable $unmappedFunctionOnObject = null
    ): self {
        $this->spreadsheetColumns[] = new SpreadsheetColumn(
            $propertyName,
            new ColumnConfiguration(
                $columnTitleLabel,
                $formatFunctionOnProperty !== null ? Closure::fromCallable($formatFunctionOnProperty) : null,
                $phpOfficeFormatCode,
                $unmappedStaticValue,
                $unmappedFunctionOnObject !== null ? Closure::fromCallable($unmappedFunctionOnObject) : null
            )
        );
        return $this;
    }

    /**
     * @return iterable<SpreadsheetColumn>
     */
    public function iterateColumns(): iterable
    {
        foreach($this->spreadsheetColumns as $column){
            yield $column;
        }
    }

    /**
     * @return string[]
     */
    public function getSortedPropertyNames(): array
    {
        $sortedNames = [];
        foreach($this->spreadsheetColumns as $column){
            $sortedNames[] = $column->getPropertyName();
        }
        return $sortedNames;
    }

    /**
     * @return array<\Closure>
     */
    public function getCallablesAsArray(): array
    {
        $listCallables = [];
        foreach($this->spreadsheetColumns as $column){
            $closure = $column->getColumnConfiguration()->getFormatFunctionOnProperty();
            if($closure !== null){
                $listCallables[$column->getPropertyName()] = $closure;
            }
        }
        return $listCallables;
    }
}
