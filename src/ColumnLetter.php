<?php

namespace OpenbyteSpreadsheetExport;

class ColumnLetter
{
    public const ALPHABET = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q',
        'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

    private int $currentIndex = 0;

    public function __construct(){
    }

    public function isLastLetter(): bool
    {
        return $this->currentIndex === count(self::ALPHABET) - 1;
    }

    public function incrementLetter(): void
    {
        $this->currentIndex++;
        if(!isset(self::ALPHABET[$this->currentIndex])){
            $this->currentIndex = 0;
        }
    }

    public function __toString(): string
    {
        return self::ALPHABET[$this->currentIndex];
    }
}