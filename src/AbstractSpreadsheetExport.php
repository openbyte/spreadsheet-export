<?php

namespace OpenbyteSpreadsheetExport;

use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Mapping\Factory\ClassMetadataFactory;
use Symfony\Component\Serializer\Mapping\Loader\AttributeLoader;
use Symfony\Component\Serializer\NameConverter\MetadataAwareNameConverter;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Serializer;

abstract class AbstractSpreadsheetExport implements ReportInterface
{
    private ?SpreadsheetBuilder $builder = null;

    /** @var object[] */
    private array $listEntitiesToWrite = [];

    /**
     * @param SpreadsheetBuilder $builder
     * @param array<string, mixed> $options
     * @return SpreadsheetBuilder
     */
    abstract protected function buildSpreadsheet(SpreadsheetBuilder $builder, array $options): SpreadsheetBuilder;

    /**
     * @return string[]
     */
    abstract protected function getGroupNames(): array;

    /**
     * @param object[] $listEntitiesToWrite
     * @return void
     */
    public function setListEntitiesToWrite(array $listEntitiesToWrite): void
    {
        $this->listEntitiesToWrite = $listEntitiesToWrite;
    }

    /**
     * @return object[]
     */
    public function getRowsToWrite(): array
    {
        return $this->listEntitiesToWrite;
    }

    /**
     * @param array<string, mixed> $options
     * @return Spreadsheet
     * @throws \Exception
     */
    public function getSpreadSheet(array $options = []): Spreadsheet
    {
        $this->builder = $this->buildSpreadsheet(new SpreadsheetBuilder(), $options);
        $listOfEntities = $this->getRowsToWrite();

        $serializer = $this->getSerializer();
        $csvContent = $serializer->serialize($listOfEntities, CsvEncoder::FORMAT, $this->getSerializerContext());

        $csvReader = new Csv();
        $spreadsheet = $csvReader->loadSpreadsheetFromString($csvContent);

        $sheet = $spreadsheet->getActiveSheet();
        $columnIndex = new ColumnIndex();
        $builder = $this->getSpreadsheetBuilder();
        foreach ($builder->iterateColumns() as $column) {
            $columnOptions = $column->getColumnConfiguration();

            $sheet = $this->formatCellsInSpreadsheet($sheet, $columnOptions, $columnIndex);
            $sheet = $this->writeCustomHeaderTitleInSpreadsheet($sheet, $columnOptions, $columnIndex);
            $sheet = $this->writeStaticValues($sheet, $columnOptions, $columnIndex, $listOfEntities);

            $columnIndex->incrementColumnLetter();
        }

        $styleHeader = ['font' => ['bold' => true]];
        $sheet->getStyle('1:1')
            ->applyFromArray($styleHeader)
            ->getNumberFormat()
            ->setFormatCode(NumberFormat::FORMAT_TEXT);
        return $spreadsheet;
    }

    /**
     * @return Serializer
     * @throws \Exception
     */
    private function getSerializer(): Serializer
    {
        $builder = $this->getSpreadsheetBuilder();
        $classMetadataFactory = new ClassMetadataFactory(new AttributeLoader());
        $metadataAwareNameConverter = new MetadataAwareNameConverter($classMetadataFactory);

        $getSetMethodContext = [
            AbstractNormalizer::CALLBACKS => $builder->getCallablesAsArray(),
        ];
        $getSetNormaliser = new GetSetMethodNormalizer(
            $classMetadataFactory,
            $metadataAwareNameConverter,
            null,
            null,
            null,
            $getSetMethodContext
        );
        return new Serializer(
            [$getSetNormaliser, new ArrayDenormalizer()],
            [new CsvEncoder()]
        );

    }

    /**
     * @return array<string, mixed>
     * @throws \Exception
     */
    private function getSerializerContext(): array
    {
        $builder = $this->getSpreadsheetBuilder();
        $serializerContext = [
            'groups' =>  $this->getGroupNames(),
            AbstractNormalizer::ATTRIBUTES => $builder->getSortedPropertyNames()
        ];
        $serializerContext[CsvEncoder::HEADERS_KEY] = $builder->getSortedPropertyNames();
        return $serializerContext;
    }

    private function formatCellsInSpreadsheet(
        Worksheet $worksheet,
        ColumnConfiguration $config,
        string $columnLetter
    ): Worksheet {
        $formatCode = $config->getPhpOfficeFormatCode();
        if ($formatCode !== null) {
            $worksheet
                ->getStyle(sprintf('%s:%s', $columnLetter, $columnLetter))
                ->getNumberFormat()
                ->setFormatCode($formatCode);
        }
        return $worksheet;
    }

    /**
     * @param Worksheet $worksheet
     * @param ColumnConfiguration $config
     * @param string $columnLetter
     * @return Worksheet
     */
    private function writeCustomHeaderTitleInSpreadsheet(
        Worksheet $worksheet,
        ColumnConfiguration $config,
        string $columnLetter
    ): Worksheet {
        $rowNumber = 1;
        $headerName = $config->getColumnTitleLabel();
        if($headerName !== null){
            $cellCoordinate = sprintf('%s%d', $columnLetter, $rowNumber);
            $worksheet
                ->getCell($cellCoordinate)
                ->getStyle()
                ->getNumberFormat()
                ->setFormatCode(NumberFormat::FORMAT_TEXT);
            $worksheet->setCellValue($cellCoordinate, $headerName);
        }
        return $worksheet;
    }

    /**
     * @param Worksheet $worksheet
     * @param ColumnConfiguration $config
     * @param string $columnLetter
     * @param object[] $listOfEntities
     * @return Worksheet
     * @throws \Exception
     */
    private function writeStaticValues(
        Worksheet $worksheet,
        ColumnConfiguration $config,
        string $columnLetter,
        array $listOfEntities
    ): Worksheet {
        $staticValue = $config->getUnmappedStaticValue();
        $functionStaticValues = $config->getUnmappedFunctionOnObject();

        if ($staticValue !== null || $functionStaticValues !== null) {
            $rowIndex = 2;
            foreach($listOfEntities as $entity){
                $value = null;
                if($staticValue !== null){
                    $value = $staticValue;
                } elseif ($functionStaticValues !== null){
                    $value = $functionStaticValues($entity);
                }
                if($value !== null) {
                    $worksheet->setCellValue(sprintf("%s%d", $columnLetter, $rowIndex), $value);
                }
                $rowIndex++;
            }
        }
        return $worksheet;
    }

    private function getSpreadsheetBuilder(): SpreadsheetBuilder
    {
        $builder = $this->builder;
        if($builder === null){
            throw new \Exception("builder shouldn't be accessed before it is set");
        }
        return $builder;
    }
}
