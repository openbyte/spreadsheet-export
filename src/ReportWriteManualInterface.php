<?php

namespace OpenbyteSpreadsheetExport;

use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

interface ReportWriteManualInterface extends ReportInterface
{
    /**
     * @param Worksheet $sheet
     * @param array<string|int, mixed> $listEntitiesToWrite
     * @param array<string, mixed> $options
     * @return void
     */
    public function writeEntriesToSheet(Worksheet $sheet, array $listEntitiesToWrite, array $options): void;
}