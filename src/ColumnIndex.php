<?php

namespace OpenbyteSpreadsheetExport;

class ColumnIndex
{
    /** @var ColumnLetter[]  */
    private array $listColumn;

    public function __construct(){
        $this->listColumn = [new ColumnLetter()];
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function incrementColumnLetter(): void
    {
        $this->incrementColumnLetterByIndex();
    }

    /**
     * @param int $index
     * @return void
     * @throws \Exception
     */
    private function incrementColumnLetterByIndex(int $index = 0): void
    {
        if(!isset($this->listColumn[$index])){
            throw new \Exception("invalid index provided");
        }

        $columnLetter = $this->listColumn[$index];
        if($columnLetter->isLastLetter()){
            if(isset($this->listColumn[$index + 1])){
                $this->incrementColumnLetterByIndex($index + 1);
            } else {
                $this->listColumn[$index + 1] = new ColumnLetter();
            }
        }
        $columnLetter->incrementLetter();
    }

    public function __toString(): string
    {
        $reversedColumnListAsString = array_map(fn(ColumnLetter $l) => (string)$l, array_reverse($this->listColumn));
        return implode('', $reversedColumnListAsString);
    }
}