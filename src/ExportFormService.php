<?php

namespace OpenbyteSpreadsheetExport;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\BaseWriter;
use PhpOffice\PhpSpreadsheet\Writer\Csv;
use PhpOffice\PhpSpreadsheet\Writer\Ods;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

class ExportFormService
{
    private const FORM_NAME = 'reportFormat';

    /**
     * @param FormBuilderInterface $formBuilder
     * @param bool $isMapped
     * @return FormBuilderInterface
     */
    public function appendFormBuilder(FormBuilderInterface $formBuilder, bool $isMapped = true): FormBuilderInterface
    {
        return $formBuilder->add(self::FORM_NAME, ChoiceType::class, array(
            "label" => "label.export.format",
            'multiple' => false,
            'expanded' => false,
            'mapped' => $isMapped,
            'choices' => [
                'Excel (xlsx)' => SpreadsheetExportFormat::ExcelFormat->value,
                'OpenDocument Spreadsheets (ods)' => SpreadsheetExportFormat::OpenDocsFormat->value,
                'comma-separated-values (csv)' => SpreadsheetExportFormat::CsvFormat->value
            ]));
    }

    /**
     * @param FormInterface $form
     * @param ReportInterface $exportService
     * @param string $filenameWithoutExtension
     * @param bool $isMapped
     * @param array<string, mixed> $spreadsheetOptions
     * @return Response
     * @throws \Exception
     */
    public function getSpreadsheetAsResponseFromForm(
        FormInterface $form,
        ReportInterface $exportService,
        string $filenameWithoutExtension,
        bool $isMapped = true,
        array $spreadsheetOptions = []
    ): Response {
        if($isMapped){
            $format = $form->getData()[self::FORM_NAME];
        } else {
            /** @var FormInterface $formField */
            $formField = $form[self::FORM_NAME];
            $format = $formField->getData();
        }
        return (new ExportOutputService())->getSpreadsheetAsResponse(
            SpreadsheetExportFormat::from($format),
            $exportService,
            $filenameWithoutExtension,
            $spreadsheetOptions
        );
    }
}