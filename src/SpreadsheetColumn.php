<?php

namespace OpenbyteSpreadsheetExport;

class SpreadsheetColumn
{
    public function __construct(private string $propertyName, private ColumnConfiguration $columnConfiguration)
    {
    }

    /**
     * @return string
     */
    public function getPropertyName(): string
    {
        return $this->propertyName;
    }

    /**
     * @return ColumnConfiguration
     */
    public function getColumnConfiguration(): ColumnConfiguration
    {
        return $this->columnConfiguration;
    }
}