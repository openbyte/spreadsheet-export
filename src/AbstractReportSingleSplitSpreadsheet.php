<?php

namespace OpenbyteSpreadsheetExport;

use PhpOffice\PhpSpreadsheet\Spreadsheet;

abstract class AbstractReportSingleSplitSpreadsheet implements ReportWriteManualInterface
{
    /**
     * @param array<string, mixed> $options
     * @return Spreadsheet[]
     */
    public function getSpreadSheet(array $options): array
    {
        $listSpreadSheet = [];
        foreach ($this->getRowsPerSpreadsheetIterator() as $rowsToWrite) {
            $spreadsheet = new Spreadsheet();
            $sheet = $spreadsheet->getActiveSheet();
            $this->writeEntriesToSheet($sheet, $rowsToWrite, $options);
            $listSpreadSheet[] = $spreadsheet;
        }
        return $listSpreadSheet;
    }

    /**
     * @return iterable<array<string|int, mixed>>
     */
    protected function getRowsPerSpreadsheetIterator(): iterable
    {
        $listRowsToWrite = $this->getRowsToWrite();
        $listCurrentBatch = [];
        foreach ($listRowsToWrite as $rowToWrite) {
            if(count($listCurrentBatch) >= $this->getNofRowsInSpreadsheet()){
                yield $listCurrentBatch;
                $listCurrentBatch = [];
            }
            $listCurrentBatch[] = $rowToWrite;
        }
        if(count($listCurrentBatch) > 0){
            yield $listCurrentBatch;
        }
    }

    protected abstract function getNofRowsInSpreadsheet(): int;
}