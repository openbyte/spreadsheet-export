<?php

namespace OpenbyteSpreadsheetExport;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

abstract class AbstractReportSingleSheet implements ReportWriteManualInterface
{
    /**
     * @param array<string, mixed> $options
     * @return Spreadsheet
     */
    public function getSpreadSheet(array $options): Spreadsheet
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $this->writeEntriesToSheet($sheet, $this->getRowsToWrite(), $options);
        return $spreadsheet;
    }
}