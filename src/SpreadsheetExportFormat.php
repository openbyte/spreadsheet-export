<?php

namespace OpenbyteSpreadsheetExport;

enum SpreadsheetExportFormat: string
{
    case CsvFormat = 'csv';
    case ExcelFormat = 'xlsx';
    case OpenDocsFormat = 'ods';

    public function getContentType(): string
    {
        return match ($this) {
            self::OpenDocsFormat => 'application/vnd.oasis.opendocument.spreadsheet',
            self::ExcelFormat => 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            self::CsvFormat => 'text/csv',
        };
    }
}
