<?php

require "vendor/autoload.php";

use OpenbyteSpreadsheetExport\AbstractSpreadsheetExport;
use OpenbyteSpreadsheetExport\ExportFormService;
use OpenbyteSpreadsheetExport\SpreadsheetBuilder;
use OpenbyteSpreadsheetExport\SpreadsheetExportFormat;

class Comment {
    #[\Symfony\Component\Serializer\Annotation\Groups("test1")]
    public string $caption;

    public function getCaption() :string {return $this->caption;}
}

class User
{
    #[\Symfony\Component\Serializer\Annotation\Groups("test1")]
    public string $familyName;

    #[\Symfony\Component\Serializer\Annotation\Groups("test1")]
    public string $firstnameName;

    public string $company;

    #[\Symfony\Component\Serializer\Annotation\Groups("test1")]
    public Comment $comment;

    /**
     * @return string
     */
    public function getFamilyName(): string { return $this->familyName; }

    /**
     * @return string
     */
    public function getFirstnameName(): string { return $this->firstnameName; }

    /**
     * @return Comment
     */
    public function getComment(): Comment { return $this->comment; }

    /**
     * @return string
     */
    public function getCompany(): string { return $this->company; }
}

class Export extends AbstractSpreadsheetExport
{
    protected function buildSpreadsheet(SpreadsheetBuilder $builder, array $options): SpreadsheetBuilder
    {
        return $builder
            ->add('familyName', 'Family Name')
            ->add("testtest", "test static",
                null,
                null,
                null,
                function(User $user): string {
                    return $user->getCompany();
                }
            )
            ->add('comment.caption', "Kommentar")
            ;
    }

    protected function getGroupNames(): array
    {
        return ["test1"];
    }
}
$comment = new Comment();
$comment->caption = "Test Caption";

$user = new User();
$user->familyName = "Kittelmann";
$user->firstnameName = "Florian";
$user->company = "openbyte GmbH";
$user->comment = $comment;

$user2 = new User();
$user2->familyName = "Kittelmann2";
$user2->firstnameName = "Florian2";
$user2->company = "openbyte GmbH2";
$user2->comment = $comment;

$user3 = new User();
$user3->familyName = "Kittelmann3";
$user3->firstnameName = "Florian3";
$user3->company = "openbyte GmbH3";
$user3->comment = $comment;

$list = [$user, $user2, $user3];
shuffle($list);
$export = new Export();
$export->setListEntitiesToWrite($list);
(new ExportFormService())->saveSpreadsheetToFile(
    SpreadsheetExportFormat::CsvFormat,
    (new Export()),
    sprintf("%s/%s", getcwd(), "test-output"),
    "testfile"
);

echo "<h1>Success!!!</h1>";

